const request = require('supertest');
const app = require('../app');

describe('TestCheckUser1', function() {
  it('has the default page', function(done) {
    request(app)
      .get('/user1')
      .expect({"Point":2000}, done);
  });
}); 

describe('TestCheckUser2', function() {
  it('has the default page', function(done) {
    request(app)
      .get('/user2')
      .expect({"Point":5000}, done);
  });
}); 


describe('TestCheckNoUser', function() {
  it('has the default page', function(done) {
    request(app)
      .get('/user3')
      .expect({"RESP":"Not found user"}, done);
  });
}); 

describe('TestCheckNoPath', function() {
  it('has the default page', function(done) {
    request(app)
      .get('/')
      .expect(/Welcome to Store/, done);
  });
}); 